using RPGCharacters.Characters;
using RPGCharacters.Custom_Exceptions;
using RPGCharacters.Items;
using System;
using Xunit;

namespace RPGCharactersTests
{
    public class CharactersTests
    {
        #region Creation
        [Fact]
        public void Constructor_CreateWarriorWithName_ShouldCreateLevelOneWarrior()
        {
            // Arrange 
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            int levelWhenCreated = 1;
            int expected = levelWhenCreated;
            // Act
            int actual = warrior1.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_CreateWarriorWithName_ShouldCreatePrimaryAndSecondaryAttributes()
        {
            // Arrange
            string name = "Test_warrior";
            Warrior warrior1 = new Warrior(name);

            int strength = 5;
            int dexterity = 2;
            int intelligence = 1;
            int vitality = 10;
            int healt = vitality * 10;
            int armorRating = strength + dexterity;
            int elementResistance = intelligence;

            int[] expected = new int[] { strength, dexterity,
                intelligence, vitality, healt, armorRating, elementResistance };

            // Act
            int[] actual = new int[] {warrior1.BasePrimaryAttribute.Strength, warrior1.BasePrimaryAttribute.Dexterity,
            warrior1.BasePrimaryAttribute.Intelligence, warrior1.BasePrimaryAttribute.Vitality,
            warrior1.SecondaryAttribute.Healt, warrior1.SecondaryAttribute.ArmorRating,
            warrior1.SecondaryAttribute.ElementResistance};
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_CreateMageWithName_ShouldCreatePrimaryAndSecondaryAttributes()
        {
            // Arrange
            string name = "Test_mage";
            Mage mage1 = new Mage(name);

            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            int vitality = 5;
            int healt = vitality * 10;
            int armorRating = strength + dexterity;
            int elementResistance = intelligence;

            int[] expected = new int[] { strength, dexterity,
                intelligence, vitality, healt, armorRating, elementResistance };

            // Act
            int[] actual = new int[] {mage1.BasePrimaryAttribute.Strength, mage1.BasePrimaryAttribute.Dexterity,
            mage1.BasePrimaryAttribute.Intelligence, mage1.BasePrimaryAttribute.Vitality,
            mage1.SecondaryAttribute.Healt, mage1.SecondaryAttribute.ArmorRating,
            mage1.SecondaryAttribute.ElementResistance};
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_CreateRangerWithName_ShouldCreatePrimaryAndSecondaryAttributes()
        {
            // Arrange
            string name = "Test_ranger";
            Ranger ranger1 = new Ranger(name);

            int strength = 1;
            int dexterity = 7;
            int intelligence = 1;
            int vitality = 8;
            int healt = vitality * 10;
            int armorRating = strength + dexterity;
            int elementResistance = intelligence;

            int[] expected = new int[] { strength, dexterity,
                intelligence, vitality, healt, armorRating, elementResistance };

            // Act
            int[] actual = new int[] {ranger1.BasePrimaryAttribute.Strength, ranger1.BasePrimaryAttribute.Dexterity,
            ranger1.BasePrimaryAttribute.Intelligence, ranger1.BasePrimaryAttribute.Vitality,
            ranger1.SecondaryAttribute.Healt, ranger1.SecondaryAttribute.ArmorRating,
            ranger1.SecondaryAttribute.ElementResistance};
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_CreateRogueWithName_ShouldCreatePrimaryAndSecondaryAttributes()
        {
            // Arrange
            string name = "Test_rogue";
            Rogue rogue1 = new Rogue(name);

            int strength = 2;
            int dexterity = 6;
            int intelligence = 1;
            int vitality = 8;
            int healt = vitality * 10;
            int armorRating = strength + dexterity;
            int elementResistance = intelligence;

            int[] expected = new int[] { strength, dexterity,
                intelligence, vitality, healt, armorRating, elementResistance };

            // Act
            int[] actual = new int[] {rogue1.BasePrimaryAttribute.Strength, rogue1.BasePrimaryAttribute.Dexterity,
            rogue1.BasePrimaryAttribute.Intelligence, rogue1.BasePrimaryAttribute.Vitality,
            rogue1.SecondaryAttribute.Healt, rogue1.SecondaryAttribute.ArmorRating,
            rogue1.SecondaryAttribute.ElementResistance};
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region LevelUp

        [Fact]
        public void LevelUp_LevelUpCharacterOnce_CharactersLevelShouldIncreaseByOne()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            warrior1.LevelUp();
            int levelAfterLevelUp = 2;
            int expected = levelAfterLevelUp;

            // Act
            int actual = warrior1.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpCharacterMultipleTimes_CharactersLevelShouldIncreaseByMultiple()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            warrior1.LevelUp(5);
            int levelAfterLevelUp = 6;
            int expected = levelAfterLevelUp;

            // Act
            int actual = warrior1.Level;

            // Assert
            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_TryToGainZeroOrNegativeLevels_ShouldThrowArgumentExceptoin(int level)
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            // Act and Assert
            Assert.Throws<ArgumentException>(() => warrior1.LevelUp(level));
        }
        

        [Fact]
        public void LevelUp_LevelUpWarriorOnce_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            int strengthOnLevelTwo = 8;
            int dexterityOnLevelTwo = 4;
            int intelligenceOnLevelTwo = 2;
            int vitalityOnLevelTwo = 15;
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            warrior1.LevelUp();
            int[] expected = new int[] {strengthOnLevelTwo, dexterityOnLevelTwo, intelligenceOnLevelTwo,
            vitalityOnLevelTwo};
            // Act
            int[] actual = new int[] {warrior1.BasePrimaryAttribute.Strength, warrior1.BasePrimaryAttribute.Dexterity,
            warrior1.BasePrimaryAttribute.Intelligence, warrior1.BasePrimaryAttribute.Vitality};
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpMageOnce_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            int strengthOnLevelTwo = 2;
            int dexterityOnLevelTwo = 2;
            int intelligenceOnLevelTwo = 13;
            int vitalityOnLevelTwo = 8;
            string name = "Test_Mage";
            Mage mage1 = new Mage(name);
            mage1.LevelUp();
            int[] expected = new int[] {strengthOnLevelTwo, dexterityOnLevelTwo, intelligenceOnLevelTwo,
            vitalityOnLevelTwo};
            // Act
            int[] actual = new int[] {mage1.BasePrimaryAttribute.Strength, mage1.BasePrimaryAttribute.Dexterity,
            mage1.BasePrimaryAttribute.Intelligence, mage1.BasePrimaryAttribute.Vitality};
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpRogueOnce_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            int strengthOnLevelTwo = 3;
            int dexterityOnLevelTwo = 10;
            int intelligenceOnLevelTwo = 2;
            int vitalityOnLevelTwo = 11;
            string name = "Test_Rogue";
            Rogue rogue1 = new Rogue(name);
            rogue1.LevelUp();
            int[] expected = new int[] {strengthOnLevelTwo, dexterityOnLevelTwo, intelligenceOnLevelTwo,
            vitalityOnLevelTwo};
            // Act
            int[] actual = new int[] {rogue1.BasePrimaryAttribute.Strength, rogue1.BasePrimaryAttribute.Dexterity,
            rogue1.BasePrimaryAttribute.Intelligence, rogue1.BasePrimaryAttribute.Vitality};
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpRangerOnce_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            int strengthOnLevelTwo = 2;
            int dexterityOnLevelTwo = 12;
            int intelligenceOnLevelTwo = 2;
            int vitalityOnLevelTwo = 10;
            string name = "Test_Ranger";
            Ranger ranger1 = new Ranger(name);
            ranger1.LevelUp();
            int[] expected = new int[] {strengthOnLevelTwo, dexterityOnLevelTwo, intelligenceOnLevelTwo,
            vitalityOnLevelTwo};
            // Act
            int[] actual = new int[] {ranger1.BasePrimaryAttribute.Strength, ranger1.BasePrimaryAttribute.Dexterity,
            ranger1.BasePrimaryAttribute.Intelligence, ranger1.BasePrimaryAttribute.Vitality};
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpWarriorOnce_ShouldIncreaseSecondaryStats()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            warrior1.LevelUp();

            int health = 150;
            int armorRating = 12;
            int elementalResistance = 2;

            int[] expected = new int[] { health, armorRating, elementalResistance };
            // Act
            int[] actual = new int[] { warrior1.SecondaryAttribute.Healt,
                warrior1.SecondaryAttribute.ArmorRating, warrior1.SecondaryAttribute.ElementResistance};

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(10)]
        public void LevelUp_LevelUpWarriorMultipleTime_ShouldIncreaseSecondaryStatsMultipleTimes(int levels)
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            int health = (warrior1.StartingAttributes.Vitality + warrior1.LevelUpAttributes.Vitality * levels)*10;
            int armorRating = warrior1.StartingAttributes.Strength + warrior1.StartingAttributes.Dexterity +
                warrior1.LevelUpAttributes.Strength * levels + warrior1.LevelUpAttributes.Dexterity * levels;
            int elementalResistance = warrior1.StartingAttributes.Intelligence + warrior1.LevelUpAttributes.Intelligence * levels;
            int[] expected = new int[] { health, armorRating, elementalResistance };
            warrior1.LevelUp(levels);
            // Act
            int[] actual = new int[] { warrior1.SecondaryAttribute.Healt,
                warrior1.SecondaryAttribute.ArmorRating, warrior1.SecondaryAttribute.ElementResistance};
            // Assert
            Assert.Equal(expected, actual);

        }

        #endregion
    }
}
