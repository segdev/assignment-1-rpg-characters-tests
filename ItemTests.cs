﻿using RPGCharacters.Attributes;
using RPGCharacters.Characters;
using RPGCharacters.Custom_Exceptions;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static RPGCharacters.Items.Armors;

namespace RPGCharactersTests
{
    public class ItemTests
    {
        #region Inventory - Weapon

        [Fact]
        public void EquipItem_EquippingWeapon_ShouldEquipWeaponToWeaponSlot()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            string weaponName = "Test_Weapon";
            int weaponLevel = 1;
            Slot weaponSlot = Slot.SLOT_WEAPON;
            WeaponTypes weaponType = WeaponTypes.WEAPON_SWORD;
            WeaponAttributes weaponAttribute = new WeaponAttributes(7, 1.1);
            Weapons weapon1 = new Weapons(weaponName, weaponLevel,
                weaponSlot, weaponType, weaponAttribute);

            warrior1.EquipItem(weapon1);
            Weapons expected = weapon1;

            // Act
            Weapons actual = (Weapons)warrior1.ItemInventory[Slot.SLOT_WEAPON];

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_EquippingLowerLevelWeapon_ShouldEquipWeaponToWeaponSlot()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            warrior1.LevelUp();

            string weaponName = "Test_Weapon";
            int weaponLevel = 1;
            Slot weaponSlot = Slot.SLOT_WEAPON;
            WeaponTypes weaponType = WeaponTypes.WEAPON_AXE;
            WeaponAttributes weaponAttribute = new WeaponAttributes(7, 1.1);
            Weapons weapon1 = new Weapons(weaponName, weaponLevel, weaponSlot, weaponType, weaponAttribute);

            warrior1.EquipItem(weapon1);
            Weapons expected = weapon1;

            // Act
            Weapons actual = (Weapons)warrior1.ItemInventory[Slot.SLOT_WEAPON];

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_EquippingHigherLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            string weaponName = "Test_Weapon";
            int weaponLevel = 2;
            Slot weaponSlot = Slot.SLOT_WEAPON;
            WeaponTypes weaponType = WeaponTypes.WEAPON_AXE;
            WeaponAttributes weaponAttribute = new WeaponAttributes(7, 1.1);
            Weapons weapon1 = new Weapons(weaponName, weaponLevel, weaponSlot, weaponType, weaponAttribute);

            // Act and assert
            Assert.Throws<InvalidWeaponException>(() => warrior1.EquipItem(weapon1));
        }



        [Fact]
        public void EquipItem_EquippingWrongTypeWeapon_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            string weaponName = "Test_Weapon";
            int weaponLevel = 1;
            Slot weaponSlot = Slot.SLOT_WEAPON;
            WeaponTypes weaponType = WeaponTypes.WEAPON_BOW;
            WeaponAttributes weaponAttribute = new WeaponAttributes(7, 1.1);
            Weapons weapon1 = new Weapons(weaponName, weaponLevel, weaponSlot, weaponType, weaponAttribute);

            // Act and assert
            Assert.Throws<InvalidWeaponException>(() => warrior1.EquipItem(weapon1));
        }
        #endregion

        #region Inventory - Items

        [Fact]
        public void EquipItem_EquippingArmor_ShouldEquipArmorToArmorSlot()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            string armorName = "Test_Armor";
            int armorLevel = 1;
            Slot armorSlot = Slot.SLOT_BODY;
            ArmorTypes armorType = ArmorTypes.ARMOR_PLATE;
            PrimaryAttributes armorAttributes = new PrimaryAttributes()
            {
                Vitality = 1,
                Strength = 2
            };
            Armors armor1 = new Armors(armorName, armorLevel, armorSlot, armorType, armorAttributes);

            warrior1.EquipItem(armor1, armorSlot);
            Armors expected = armor1;

            // Act
            Armors actual = (Armors)warrior1.ItemInventory[Slot.SLOT_BODY];

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_EquippingArmorWithHigherLevelThanCharacter_ShouldThrowInvalidArmorException()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            string armorName = "Test_Armor";
            int armorLevel = 2;
            Slot armorSlot = Slot.SLOT_BODY;
            ArmorTypes armorType = ArmorTypes.ARMOR_PLATE;
            PrimaryAttributes armorAttributes = new PrimaryAttributes()
            {
                Vitality = 1,
                Strength = 2
            };
            Armors armor1 = new Armors(armorName, armorLevel, armorSlot, armorType, armorAttributes);

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => warrior1.EquipItem(armor1, armorSlot));
            
        }

        [Fact]
        public void EquipItem_EquippingWrongTypeArmor_ShouldThrowInvalidArmorException()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);

            string armorName = "Test_Armor";
            int armorLevel = 1;
            Slot armorSlot = Slot.SLOT_BODY;
            ArmorTypes armorType = ArmorTypes.ARMOR_CLOTH;
            PrimaryAttributes armorAttributes = new PrimaryAttributes()
            {
                Vitality = 1,
                Strength = 2
            };
            Armors armor1 = new Armors(armorName, armorLevel, armorSlot, armorType, armorAttributes);

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => warrior1.EquipItem(armor1, armorSlot));

        }

        [Fact]
        public void EquipItem_EquipingArmor_ShouldChangeTotalStreghtAndTotalVitality()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            Armors testPlateBody = new Armors()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorTypes.ARMOR_PLATE,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            warrior1.EquipItem(testPlateBody, Slot.SLOT_HEAD);
            int expected = warrior1.BasePrimaryAttribute.Vitality + 2;
            // Act
            int actual = warrior1.TotalPrimaryAttribute.Vitality;
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region DPS

        [Fact]
        public void DPS_CreateCharacterWithoutAnyItems_ShouldCreateBasicDPS()
        {
            // Arrange
            double basicDPCForLevelOneWarrior = 1 * (1 + (5 / 100));
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            double expected = basicDPCForLevelOneWarrior;

            // Act
            double actual = warrior1.SecondaryAttribute.DPS;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DPS_CreateCharacterAndEquipWeapon_DPSShouldIncreaseFromBasic()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            Weapons testAxe = new Weapons()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_AXE,
                WeaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            warrior1.EquipItem(testAxe);
            int axeDamage = 7;
            double axeAttackSpeed = 1.1;
            double expected = (axeDamage * axeAttackSpeed) * (Convert.ToDouble(
                1 + (warrior1.TotalPrimaryAttribute.Strength / 100)));
            // Act
            double actual = warrior1.SecondaryAttribute.DPS;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DPS_CreateCharacterAndEquipWeaponAndArmor_DPSShouldIncreaseFromBasic()
        {
            // Arrange
            string name = "Test_Warrior";
            Warrior warrior1 = new Warrior(name);
            Weapons testAxe = new Weapons()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponTypes.WEAPON_AXE,
                WeaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            warrior1.EquipItem(testAxe);

            Armors testPlateBody = new Armors()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorTypes.ARMOR_PLATE,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            warrior1.EquipItem(testPlateBody, Slot.SLOT_HEAD);

            int axeDamage = 7;
            double axeAttackSpeed = 1.1;
                // expected is same as (7 * 1.1) * (1 + ((5 + 1) / 100))
            double expected = (axeDamage * axeAttackSpeed) * (Convert.ToDouble(
                1 + (warrior1.TotalPrimaryAttribute.Strength / 100)));
            // Act
            double actual = warrior1.SecondaryAttribute.DPS;

            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion
    }
}
